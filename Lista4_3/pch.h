#ifndef PCH_h
#define PCH_h
#include <iostream>
#include <map>
#include <fstream>
#include <cstdio>

using namespace std;

class AddressBook {
private:
	fstream file;
	string name, address, temp;
	map<string, string> addresses;
public:
	AddressBook();
	~AddressBook(); 
	void addRecord(string n, string a);
	void readFile();
	void changeRecord(string key, string newAddress);
};

#endif

#include "pch.h"
#include <iostream>
#include <map>
#include <fstream>
#include <cstdio>
using namespace std;

AddressBook::AddressBook()
{
	file.open("addressbook.txt", ios::in | ios::out);
	if (file.good()) {
		cout << "Pomyslnie otworzono plik, wczytywanie danych do mapy" << endl;
		while (file >> name && file >> address)
			addresses.insert(pair<string, string>(name, address));
	}
	else cout << "Wystapil blad przy otwieraniu pliku" << endl;	
}

AddressBook::~AddressBook()
{
	file.close();
	ofstream file_upd("addressbook.txt", ios::out | ios::trunc);
	for (map<string, string>::iterator i = addresses.begin(); i != addresses.end(); i++) {
		temp = i->first + " " + i->second;
		file_upd << temp << endl;
	}
	file_upd.close();
}

void AddressBook::addRecord(string n, string a)
{
	addresses.insert(pair<string, string>(n, a));
}

void AddressBook::readFile()
{
	for (map<string, string>::iterator i = addresses.begin(); i != addresses.end(); i++)
	cout << i->first << " " << i->second << endl;
	cout << endl;
}

void AddressBook::changeRecord(string key, string newAddress)
{
	map<string, string>::iterator record = addresses.find(key);
	if (record != addresses.end()) record->second = newAddress;
}

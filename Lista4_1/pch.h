#ifndef PCH_H
#define PCH_H
#include <iostream>
class complex
{
private:
	double rel;
	double ima;
public:
	complex();
	complex(double _re);
	complex(double _re, double _im);
	complex operator+(const complex& c);
	complex operator-(const complex& c);
	complex operator*(const complex& c);
	complex operator=(const complex& c);
	void sprzezenie();
	friend std::ostream& operator<< (std::ostream&, const complex& a);
};  
#endif

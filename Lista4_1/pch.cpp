#include "pch.h"

complex::complex(double _re, double _im)
{
	rel = _re;
	ima = _im;
}

complex::complex(double _re)
{
	rel = _re;
	ima = 0;
}

complex::complex()
{
	rel = 0;
	ima = 0;
}

complex complex::operator+(const complex& c)
{
	return complex(rel + c.rel, ima + c.ima);
}

complex complex::operator-(const complex& c)
{
	return complex(rel - c.rel, ima - c.ima);
}

complex complex::operator*(const complex& c)
{
	return complex(rel * c.rel - ima * c.ima, rel * c.ima + ima * c.rel);
}

complex complex::operator=(const complex& c)
{
	rel = c.rel;
	ima = c.ima;
	return complex(rel, ima);
}

void complex::sprzezenie()
{
	ima = -ima;
}


std::ostream& operator<<(std::ostream &os, const complex &a)
{
	if (a.rel != 0) os << a.rel;
	if (a.ima > 0 && a.ima != 0 && a.rel != 0) os << "+";
	if (a.ima!=0)  os << a.ima << "i ";
	return os;
}

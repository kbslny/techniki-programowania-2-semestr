#ifdef COMPLEX__h
#define COMPLEX__h
#include <iostream>

class complex
{
private:
	int rel;
	int ima;
public:
	complex(int _rel,int _ima);
	int suma_(complex& a, complex& b);
	friend std::ostream& operator<< (std::ostream& , const complex& a);
};
#endif
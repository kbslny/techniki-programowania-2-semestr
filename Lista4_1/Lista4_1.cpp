﻿#include "pch.h"
#include <iostream>

using namespace std;

int main()
{
	complex a(3, 0);
	cout <<"a: "<< a << endl;
	complex b(0, 1);
	cout <<"b: "<< b << endl;
	complex c(1, 1);
	cout <<"c: "<< c << endl;
	complex d(-1, -1);
	cout <<"d: "<< d << endl;
	complex e = a + c;
	cout <<"e = a + c: "<< e << endl;
	complex f = a - c;
	cout <<"f = a - c: " << f << endl;
	complex g = a * d;
	cout << "g = a * d: " << g << endl;
	complex h = b * c;
	cout << "h = b * c: " << h << endl;
	d.sprzezenie();
	cout << "sprzezenie d: "<< d << endl;
	
	
}

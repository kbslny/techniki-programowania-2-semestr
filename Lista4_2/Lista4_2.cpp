﻿#include <iostream>
#include "Figure.h"
#include "Ciracle.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "Lista4_2.h"

using namespace std;

int main()
{
	Figure** figures = new Figure * [3];
	figures[0] = new Ciracle(1);
	figures[1] = new Triangle(1, 1);
	figures[2] = new Rectangle(2, 3);
	for (int i = 0; i < 3; i++) {
		cout << *figures[i] << endl;
	}
	cout << "Pola figur:" << endl;
	for (int i = 0; i < 3; i++) {
		cout << figures[i]->area() << endl;
	}
	for (int i = 0; i < 3; i++) {
		delete figures[i];
	}
	delete[] figures;
}

#include "Triangle.h"


Triangle::Triangle(double _a, double _h)
{
	a = _a;
	h = _h;
}

double Triangle::area() const
{
	return (a*h)/2;
}

std::ostream& Triangle::print(std::ostream& out) const
{
	out << " trojkontem i mam a: " << a << " i h: " << h;
	return out;
}

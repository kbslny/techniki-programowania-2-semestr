#ifndef Rectangle_h
#define Rectangle_h
#include "Figure.h"
class Rectangle : public Figure
{
	private:
		double a;
		double b;
	public:
		Rectangle(double a,double b);
		double area() const;
		std::ostream& print(std::ostream& out) const;
};
#endif
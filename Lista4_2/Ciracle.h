#ifndef Ciracle_h
#define Ciracle_h
#include "Figure.h"
class Ciracle : public Figure
{
	private:
		double r;
	public:
		Ciracle(double r);
		double area() const;
		std::ostream& print(std::ostream& out) const;
};
#endif
#ifndef Triangle_h
#define Triangle_h
#include "Figure.h"
class Triangle : public Figure
{
private:
	double a;
	double h;
public:
	Triangle(double a, double h);
	double area() const;
	std::ostream& print(std::ostream& out) const;
};
#endif
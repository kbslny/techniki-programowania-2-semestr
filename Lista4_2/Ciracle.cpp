#include "Ciracle.h"

Ciracle::Ciracle(double _r)
{
	r = _r;
}

double Ciracle::area() const
{
	return 3.14*r*r ;
}

std::ostream& Ciracle::print(std::ostream& out) const
{
	out << " kolem i mam r: " << r;
	return out;
}

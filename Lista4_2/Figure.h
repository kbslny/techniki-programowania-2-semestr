#include <iostream>
#ifndef Figure_h
#define Figure_h
class Figure
{
	public:
		virtual double area() const = 0;
		virtual std::ostream& print(std::ostream& out) const = 0;
		friend std::ostream& operator<<(std::ostream& out, Figure& f);
};

#endif